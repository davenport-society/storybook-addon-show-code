const isTest = (process.env.BABEL_ENV || '').toLowerCase() === 'test'
const isDev = (process.env.NODE_ENV || '').toLowerCase() === 'development'
const isProd = (process.env.NODE_ENV || '').toLowerCase() === 'production'
const development = isDev || isTest

const prodPlugins = ['groundskeeper-willie', '@babel/plugin-transform-react-constant-elements']

const config = {
   comments: false,
   presets: [
      'airbnb',
      '@babel/preset-typescript',
      ['@babel/preset-env'],
      ['@babel/preset-react', { development }]
   ],
   plugins: ['@babel/plugin-syntax-dynamic-import']
}

if (isProd) config.plugins.concat(prodPlugins)

module.exports = config
