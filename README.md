# storybook-addon-show-code

![React Storybook addon](assets/info.gif)

_Adapted from https://github.com/SOFTVISION-University/storybook-addon-code_

#### Why?

Supports:

-  all prismjs supported languages
-  primjs plugins
-  prismjs compatible themes
-  multiple code panels of the same type

---

A Storybook addon enabling to show off code samples in the Storybook panel for your stories in [Storybook](https://storybook.js.org).

### Getting Started

```sh
npm i --save-dev storybook-addon-show-code
yarn add -D storybook-addon-show-code
```

### Usage

Create a file called `addons.js` in your storybook config.

Add following content to it:

```js
import { setTabs } from 'storybook-addon-show-code/register'
import { loadLanguages } from 'storybook-addon-show-code/configure'

// directly import desired theme
import 'prism-themes/themes/prism-darcula.css'

// directly import plugins with any css it requires
import 'prismjs/plugins/line-numbers/prism-line-numbers'
import 'prismjs/plugins/line-numbers/prism-line-numbers.css'

loadLanguages(['tsx', 'typescript'])

setTabs([{ label: 'JSX', type: 'tsx' }, { label: 'Typescript', type: 'typescript' }])
```

`loadLanguages` accepts an array of [prismjs supported languages](https://prismjs.com/#supported-languages)

`setTab` accepts an array of objects for the tabs you wish to create. The label will be displayed in the Storybook panel as the name of the tab.

Then write your stories like this:

```js
import { storiesOf } from '@storybook/react'
import withCode from 'storybook-addon-code'
import Button from './Button'

const styleFile = require('raw-loader!./style.scss')
const typescriptFile = require('./test.tsx')

storiesOf('Button', module)
   .addDecorator(showCode())
   .add('myComponent', () => <Component />, {
      showCode: [
         {
            label: 'JSX',
            code: require('!raw-loader!./Component.tsx').default
         },
         {
            label: 'TESTS',
            code: require('!raw-loader!./Component.spec.tsx').default
         }
      ]
   })
```
