/* eslint-disable @typescript-eslint/no-var-requires */
const gulp = require('gulp')
const babel = require('gulp-babel')
const clean = require('gulp-clean')
const babelrc = require('./.babelrc')

const builtFilenames = ['configure.js', 'index.js', 'register.js', 'styles.css']
gulp.task('build', () =>
   gulp
      .src('src/*.ts?(x)')
      .pipe(babel(babelrc))
      .pipe(gulp.dest('.'))
)

gulp.task('css', () => gulp.src('src/*.css').pipe(gulp.dest('.')))

gulp.task('clean', () => gulp.src(`@(${builtFilenames.join('|')})`).pipe(clean()))

gulp.task('default', gulp.series('clean', 'build', 'css'))

gulp.task('dev', () => gulp.watch('src', gulp.series('default')))
