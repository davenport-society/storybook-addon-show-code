module.exports = {
   parser: '@typescript-eslint/parser',
   plugins: ['sonarjs', '@typescript-eslint', 'react-hooks'],
   extends: [
      'airbnb-base',
      'xo',
      'xo-space/esnext',
      'plugin:unicorn/recommended',
      'plugin:react/recommended', // Uses the recommended rules from @eslint-plugin-react
      'plugin:sonarjs/recommended',
      'plugin:@typescript-eslint/recommended', // Uses the recommended rules from @typescript-eslint/eslint-plugin
      'prettier/@typescript-eslint', // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
      'plugin:prettier/recommended' // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
   ],
   parserOptions: {
      ecmaVersion: 8,
      sourceType: 'module', // Allows for the use of imports
      ecmaFeatures: {
         jsx: true
      }
   },
   env: {
      browser: true,
      node: true,
      es6: true
   },
   rules: {
      complexity: ['error', 11],
      'react-hooks/rules-of-hooks': 'error',
      'react-hooks/exhaustive-deps': 'warn',
      'react/display-name': 0,
      'no-console': 0,
      'no-param-reassign': 0,
      'capitalized-comments': 'off',
      'unicorn/filename-case': ['error', { cases: { camelCase: true, pascalCase: true } }],
      'unicorn/new-for-builtins': 0,
      'unicorn/prevent-abbreviations': 0,
      'valid-jsdoc': 0,
      'new-cap': 0,
      'no-warning-comments': ['error', { terms: ['?'], location: 'start' }],
      'react/prop-types': 0,
      'no-underscore-dangle': 0,
      'import/prefer-default-export': 0,
      'import/no-unresolved': 0,
      'import/no-extraneous-dependencies': 0,
      '@typescript-eslint/explicit-function-return-type': [0, { allowExpressions: true }],
      '@typescript-eslint/await-promise': true,
      '@typescript-eslint/deprecation': true,
      '@typescript-eslint/import-name': false,
      '@typescript-eslint/max-classes-per-file': [true, 1],
      '@typescript-eslint/member-access': true,
      '@typescript-eslint/prefer-interface': 0,
      '@typescript-eslint/no-boolean-literal-compare': true,
      '@typescript-eslint/no-default-export': true,
      '@typescript-eslint/no-invalid-this': true,
      '@typescript-eslint/no-mergeable-namespace': true,
      '@typescript-eslint/no-return-await': true,
      '@typescript-eslint/no-unused-vars': 0,
      '@typescript-eslint/no-unsafe-any': false,
      '@typescript-eslint/no-explicit-any': 0,
      '@typescript-eslint/no-non-null-assertion': 0,
      '@typescript-eslint/prefer-conditional-expression': [true, 'check-else-if'],
      '@typescript-eslint/prefer-object-spread': true,
      '@typescript-eslint/variable-name': [
         true,
         'check-format',
         'ban-keywords',
         'allow-leading-underscore',
         'allow-pascal-case'
      ]
   },
   settings: {
      react: {
         version: 'detect' // Tells eslint-plugin-react to automatically detect the version of React to use
      }
   }
}
