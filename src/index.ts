import addons, { makeDecorator } from '@storybook/addons'

type CodeOpts = {
   label: string
   code: string
}
export default makeDecorator({
   name: 'showCode',
   parameterName: 'showCode',
   // This means don't run this decorator if the notes decorator is not set
   skipIfNoParametersOrOptions: true,
   wrapper: (getStory, context, { parameters, options }) => {
      const channel = addons.getChannel()
      const codeList = parameters || options

      codeList.forEach((codeOpts: CodeOpts) =>
         channel.emit(`showCode/add_${codeOpts.label.toUpperCase()}`, {
            ...codeOpts,
            ctext: context,
            channel
         })
      )

      return getStory(context)
   }
})
