import components from 'prismjs/components'

const { languages } = components
const { path } = languages.meta

delete languages.meta

const getPath = (dep: string) => path.replace('{id}', dep)

const aliases = Object.keys(languages).reduce((aliased: any, nextLang) => {
   const { alias } = languages[nextLang]
   const aliasList = Array.isArray(alias) ? alias : [alias]

   aliasList.forEach((a) => {
      aliased[a] = languages[nextLang]
   })

   return aliased
}, {})

const withAliases = { ...languages, ...aliases }
const allLangs: string[] = Object.keys(withAliases)

const verifyLanguages = (arr: string[]) => {
   let notFound = ''
   for (const lang of arr) {
      if (!allLangs.includes(lang)) notFound += `${lang} `
   }

   return notFound
}

const loadRequires = (requires: string | string[]) => {
   if (Array.isArray(requires)) requires.forEach((r) => import(`prismjs/${getPath(r)}`))
   else import(`prismjs/${getPath(requires)}`)
}

const loadLanguages = (arr = allLangs) => {
   if (arr.length === 0) {
      console.error(
         '[storybook showCode] loadLanguages() was called with an empty array. No languages loaded'
      )
      return
   }

   const notFound = verifyLanguages(arr)
   if (notFound) {
      console.error(`[storybook showCode] no languages were loaded:

You provided the following unsupported languages: ${notFound}
Check the list of supported languages at https://prismjs.com/#supported-languages`)
      return
   }

   arr.forEach((lang) => {
      const { peerDependencies, require } = withAliases[lang]

      // load dependencies first
      if (peerDependencies !== undefined) {
         loadLanguages(Array.isArray(peerDependencies) ? peerDependencies : [peerDependencies])
      }

      if (require !== undefined) loadRequires(require)

      // if the language is not an alias, load it
      if (lang in aliases === false) import(`prismjs/${getPath(lang)}`)
   })
}

export { loadLanguages }
