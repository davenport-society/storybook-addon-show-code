import './styles.css'

import Prism from 'prismjs'
import React, { useEffect, useState } from 'react'

import addons from '@storybook/addons'
import { API } from '@storybook/api'
import Channel from '@storybook/channels'
import { STORY_CHANGED } from '@storybook/core-events'

type Props = {
   channel: Channel
   api: API
   type: string
   label: string
   active: boolean
}

// hack to make plugins work https://github.com/PrismJS/prism/issues/1487#issuecomment-406836427
const runPluginHack = () => {
   setTimeout(() => {
      Prism.highlightAll()
   }, 0)
}

const Code = ({ label, channel, type, api, active }: Props) => {
   const [formattedCode, setFormattedCode] = useState('')

   const channelName = `showCode/add_${label.toUpperCase()}`
   const data = api.getCurrentStoryData()
   const id = data ? `${label}_${data.id}` : ''

   const selectTab = ({ code }: { code: string }) => {
      const codePrism = type && code && Prism.highlight(code, Prism.languages[type], type)

      setFormattedCode(codePrism)
   }

   useEffect(() => {
      runPluginHack()
      channel.on(channelName, selectTab)
      api.on(STORY_CHANGED, () => {
         selectTab({ code: '' })
      })

      return () => {
         api.off(STORY_CHANGED, () => selectTab({ code: '' }))
         channel.removeListener(channelName, selectTab)
      }
   })

   return (
      <div>
         {active ? (
            <pre key={id} className={`language-${type} line-numbers show-code`}>
               <code className={`language-${type}`}>
                  <div dangerouslySetInnerHTML={{ __html: formattedCode }} />
               </code>
            </pre>
         ) : null}
      </div>
   )
}

type Labels = { label: string; type: string }

const registerTab = async ({ label, type }: Labels, idx: number) => {
   addons.register(`showCode/add_${label}`, (api) => {
      addons.addPanel(`showCode/${label}/panel`, {
         title: label,
         render: ({ active }) => (
            <Code
               channel={addons.getChannel()}
               key={idx}
               api={api}
               type={type}
               label={label.toUpperCase()}
               active={active}
            />
         )
      })
   })
}

export const setTabs = (tabs: Labels[]) => {
   const tabsToRender = [...tabs]
   tabsToRender.forEach((t, idx) => registerTab(t, idx))
}
